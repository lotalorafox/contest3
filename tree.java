import java.util.ArrayList;
import java.util.Scanner;
import java.math.*;

public class tree{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String labda ="";
            String beta = "";
            int repetitions =0;
            String a = sc.nextLine();
            labda = del(a);
            a = sc.nextLine();
            beta = del(a);
            //repetitions 
            int step = (labda.length());
            int comp = hash(labda);
            int h =hash(beta.substring(0,step));
            if( h == comp ){
                repetitions++;
            }
            for(int i=1;i<beta.length();i++){     
                h = h + hash(beta.substring(step,step+1));
                h = h - hash(beta.substring(i-1,i));
                h = h/3;
                System.out.println("h: " +h +"comp" + comp);
                if( h == comp ){
                    repetitions++;
                }
                //System.out.println("is less");
                //System.out.println("substring: " + beta.substring(i,i+step));
            }
            // System.out.println(labda + hash(labda));
            // System.out.println(beta + hash(beta));
            // System.out.println("repetitions " + repetitions);
            System.out.println(repetitions);
        }
    }

    //clean function
    public static String  del(String a){
        String b ="";
        for(int i=0;i<a.length();i++){
            char t = a.charAt(i);
            if(t == '+' || t == '/' || t == '-' || t == '*'){
                b += "!";
            }else if(((int)t >=97 && (int)t<=122) || ((int)t >=48 && (int)t<=57)){
                if(b.length() >= 1){
                    //System.out.println("b: " + b);
                    //System.out.println("size " + b.length() + " b on: " + (i-1));
                    if(b.charAt(b.length()-1) != 't'){
                        b +="t";
                    }
                }
            }
            else{
                b +=(t);
            }
        }
        if(b.length() ==0){
            b +="t";
        }
        return b;
    }
    public static int hash(String s){
        int p =0;
        int has =0;
        int l =0;
        for(int i=(s.length()-1);i>=0;i--){
            if(s.charAt(i) == 't'){
                l =1;
            }else if(s.charAt(i) == '!'){
                l =2;
            }else{
                l=0;
            }
            has += l*((Math.pow(3, p)));
            p++;
        }
        return has;
    }
}