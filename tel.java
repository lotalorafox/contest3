import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

public class tel{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        HashMap<String,ArrayList> people = new HashMap<>();
        int cases = sc.nextInt();
        boolean aleja = true;
        for(int i=0;i<cases;i++){
            String name = sc.next();
            ArrayList<String> tels = new ArrayList<>();
            int j = sc.nextInt();
            while(j>0){
                if(people.containsKey(name)){
                    people.get(name).add(sc.next());
                }else
                    tels.add(sc.next());
                j--;
            }
            if(!(people.containsKey(name))){
                people.put(name, tels);
            }  
        }
        Object[] ke = people.keySet().toArray();
        aleja = false;
        //order
        for(int i=0;i<ke.length;i++){
            ArrayList<String> tem = people.get(ke[i]);
            Collections.sort(tem, (a, b)->Integer.compare(b.length(), a.length()));
            //System.out.println(tem.toString());
            for(int j=0;j<tem.size();j++){
                String tema = tem.get(j);
                for(int t=(j+1);t<tem.size();t++){
                    if((tema.equals(tem.get(t))) || (tema.endsWith(tem.get(t)))){
                        //System.out.println(tema + " con " + tem.get(t));
                        tem.remove(t);
                        t--;
                        //System.out.println(tem.toString());
                    }
                }
            }
            people.remove(ke[i]);
            people.put((String)ke[i], tem);
        }

        //print
        System.out.println(people.size());
        for(int l=0;l<people.size();l++){
            System.out.print(ke[l] +" "+ (int)(people.get(ke[l]).size())+ " ");
            for(int a =0;a<people.get(ke[l]).size();a++){
                System.out.print(people.get(ke[l]).get(a) + " ");
            }
            System.out.println();
        }
    }
}